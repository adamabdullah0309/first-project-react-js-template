import React, { Component,Redirect,useState,useEffect } from 'react'
import { useCookies } from 'react-cookie'
import { Layout, Table, Button, Spin, Form, message, Tabs } from 'antd'
import ButtonMain from '../../../components/ButtonMain'
import { DeleteOutlined } from '@ant-design/icons';
import ListQuestion from './ListQuestion/ListQuestion'
import CreateQuestion from './ListQuestion/CreateQuestion'
import { List } from 'antd/lib/form/Form';
import { changeConfirmLocale } from 'antd/lib/modal/locale';
const { Content} = Layout;
const { TabPane } = Tabs;
function MasterSetting() {
    const [cookies] = useCookies(['sessionUser'])
    const [discard, setDiscard] = useState(true)
    const [create, setCreate] = useState(true)
    const [visibleForm, setVisibleForm] = useState(false)
    let [button, setButton] =  useState(
        {
            create: {
                type: 'primary',
                onClick: (e) => {
                    console.log('discard befrre',discard)
                    // form.resetFields()
                    //  setVisibleForm(true);
                     setDiscard(false);
                     if(discard)
                     {
                         console.log('true')
                     }
                     else{
                         console.log('false')
                     }
                     console.log('after befrre',discard)
                    //  setButton(btnDiscard);
                    //  change();
                },
            },
        }
    )
    
    const change = () =>{
        if(discard === true)
        {
            console.log('true')
        }
        else{
            console.log('false')
        }
        console.log('discard',discard);
    }

    const [fitur, setFitur] = useState("1.1")
    const [form] = Form.useForm()
     

    let btnConfigUsers = {
        create: {
            type: 'primary',
            onClick: (e) => {
                setVisibleForm(true);
                form.resetFields()
                setButton(btnDiscard);
            },
        },
    }

    let btnDiscard = {
        discard : {
            type: 'primary',
            onClick: (e) => {
                form.resetFields()
                setVisibleForm(false);
                setButton(btnConfigUsers);
            },
        }
    }

    

    const columns = [
        {
            title: 'Username',
            dataIndex: 'username',
            key: 'username',
        },
        {
            title: 'Name',
            dataIndex: 'fullname',
            key: 'fullname',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Phone Number',
            dataIndex: 'phone_number',
            key: 'phone_number',
        },
        // {
        //     title: 'Action',
        //     key: 'action',
        //     render: (text, record) =>
        //         dataSource.length >= 1 ? (
        //             <Button type="warning" shape="circle" icon={<EditOutlined />} onClick={() => onEditForm(record)} />
        //         ) : null,
        // },
    ]
     const onClickTab = async (key, event) =>{
        if(key == "1"){
            await setFitur("1.1")
        }
        else{
            await setFitur(key)
        }
        
    }


    return (

        <Content
            className="site-layout-background content"
        >
            <ButtonMain
                config={button}
                bottomDashed={true}
            />
            <Tabs type="card" onTabClick={(activeKey) => onClickTab(activeKey)}>
                <TabPane tab="Question" key="1">
                    <Tabs type="card" activeKey={fitur} onTabClick={(activeKey2) => onClickTab(activeKey2)} >
                        <TabPane tab="List Question" key="1.1">
                            {fitur === "1.1" && visibleForm === true ? <CreateQuestion btnConfigUsers={btnConfigUsers} /> : <ListQuestion/>}
                        </TabPane>
                        <TabPane tab="Master group" key="1.2">
                            <p>Master group</p>
                        </TabPane>
                    </Tabs>
                </TabPane>
                <TabPane tab="Template" key="2" >
                    <p>Content of Tab Pane 2</p>
                    <p>Content of Tab Pane 2</p>
                    <p>Content of Tab Pane 2</p>
                </TabPane>
                <TabPane tab="Parameter" key="3" >
                    <p>Content of Tab Pane 3</p>
                    <p>Content of Tab Pane 3</p>
                    <p>Content of Tab Pane 3</p>
                </TabPane>
            </Tabs>
            {/* <Spin tip="Loading..." spinning={loading}>
                <Table 
                    columns={columns}
                    // dataSource={dataSource}
                />
            </Spin> */}
        </Content>
    )
    
}


export default MasterSetting;
