import React, { Component,Redirect,useState,useEffect } from 'react'
import { useCookies } from 'react-cookie'
import { Layout, Table, Button, Spin, Form, message, Tabs } from 'antd'
import { EditOutlined } from '@ant-design/icons'
import { DeleteOutlined } from '@ant-design/icons';
const { Content} = Layout;
const { TabPane } = Tabs;

function ListQuestion(props)
{
    const [loading, setLoading] = useState(false)
    const [dataSource, setDataSource] = useState([])
    const onEditForm = (record) => {
        console.log('record')
    }

    const dataList = [
        {
            'id' : 'ID-12152020-0001',
            'name' : 'Pertanyaan 1',
            'status' : '1'
        },
        {
            'id' : 'ID-12152020-0002',
            'name' : 'Pertanyaan 2',
            'status' : '1'
            
        }
    ]

    const columns2 = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) =>
            dataList.length >= 1 ? (
                    <Button type="warning" shape="circle" icon={<EditOutlined />} onClick={() => onEditForm(record)} />
                ) : null,
        },
    ]

    console.log(props)
    return(
        <Spin tip="Loading..." spinning={loading}>
            <Table 
                columns={columns2}
                dataSource={dataList}
            />
        </Spin>
    )
}

export default ListQuestion;