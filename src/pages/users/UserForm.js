import React from 'react'
import { Drawer, Form, Button, Col, Row, Input } from 'antd';

function UserForm({ form, visible, onClose, onSubmit, isEdit, onUpdate }) {
    return (
        <Drawer
            title={isEdit ? "Edit a user" : "Create a new user"}
            width={720}
            onClose={onClose}
            visible={visible}
            bodyStyle={{ paddingBottom: 80 }}
            footer={
            <div
                style={{
                    textAlign: 'right',
                }}
            >
                <Button onClick={onClose} style={{ marginRight: 8 }}>
                    Cancel
                </Button>
                <Button onClick={isEdit ? onUpdate : onSubmit} type="primary">
                    Submit
                </Button>
            </div>
            }
        >
            <Form layout="vertical" form={form}>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item
                            name="fullname"
                            label="Fullname"
                            rules={[{ required: true, message: 'Please enter fullname' }]}
                        >
                            <Input placeholder="Please enter fullname" />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item
                            name="username"
                            label="Username"
                            rules={[{ required: true, message: 'Please enter username' }]}
                        >
                            <Input placeholder="Please enter username" />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item
                            name="password"
                            label={`${isEdit ? 'New ' : ''}Password`}
                            rules={!isEdit && [{ required: true, message: 'Please enter valid password' }]}
                            extra="Password at least has 1 uppercase character, 1 lowercase character, 1 digit number, 1 special character and minimum 8 characters."
                        >
                            <Input.Password placeholder="Please enter password" />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item
                            name="email"
                            label="Email"
                            rules={[{ required: true, message: 'Please enter valid email', type: 'email' }]}
                        >
                            <Input placeholder="Please enter email" />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item
                            name="phoneNumber"
                            label="Phone Number"
                        >
                            <Input placeholder="Please enter phone number" addonBefore="+62" />
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </Drawer>
    )
}

export default UserForm
