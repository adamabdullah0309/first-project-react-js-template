import React, { useState, useEffect } from 'react'
import moment from 'moment'
import { useParams, useHistory } from "react-router-dom";
import { Layout, Button, Divider, Tooltip, Row, Col, Select, DatePicker, Space, Checkbox, message, Popconfirm } from 'antd'
import { PlusOutlined, DeleteOutlined, CheckOutlined } from '@ant-design/icons'

import ButtonMain from '../../../components/ButtonMain'
import UserInfo from '../../../components/UserInfo'
import { api as helperApi, RestClient } from '../../../helpers/api'

const { Content} = Layout;
const { Option } = Select;

function UserHasGroup(props) {
    const history = useHistory();
    const { userId } = useParams()
    const [detailUser, setDetailUser] = useState({})
    const [groups, setGroups] = useState([])
    const [hasGroups, setHasGroups] = useState([])
    const [hasCountGroup, setHasCountGroup] = useState(0)

    const btnConfig = {
        discard: {
            type: 'danger',
            onClick: (e) => {
                history.push("/users/access")
            },
        }
    }

    useEffect(() => {
        helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/groups/all`, {
            headers: helperApi.hubApi.defaultHeaderReq
        }).then(resp => {
            if (resp.status) {
                setGroups(resp.data)
            }
        })
    }, [])

    useEffect(() => {
        helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/users?column=user_uuid&value=${userId}`, {
            headers: helperApi.hubApi.defaultHeaderReq
        }).then(resp => {
            if (resp.status) {
                if (resp.data.length > 0) {
                    setDetailUser(resp.data[0])
                }
            }
        })
    }, [])

    useEffect(() => {
        helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/trx/user-groups/all?user_uuid=${userId}`, {
            headers: helperApi.hubApi.defaultHeaderReq
        }).then(resp => {
            if (resp.status) {
                const userHasGroup = resp.data.map((obj, i) => ({ 
                    groupId: obj.group_id,
                    startDate: moment(obj.start_date).format('YYYY-MM-DD'),
                    endDate: moment(obj.end_date).format('YYYY-MM-DD'),
                    key: (i+1),
                    submited: true,
                    userGroupStatus: obj.status,
                    warning: false,
                    isEdited: false,
                    isCurrent: true
                }))
                setHasGroups(userHasGroup)
                setHasCountGroup(userHasGroup.length)
            }
        })
    }, [])

    const addGroup = () => {
        let countGroups = hasCountGroup
        const newCount = countGroups+1
        const currGroups = [...hasGroups, { groupId: null, startDate: '', endDate: '', key: newCount, submited: false, userGroupStatus: false, warning: false, isEdited: false, isCurrent: false }]
        setHasGroups(currGroups)
        setHasCountGroup(newCount)
    }

    const removeGroup = (data) => {
        const currGroups = hasGroups.filter(group => group.key !== data.key)
        setHasGroups(currGroups)
    }

    const onChangeGroup = (currGroup, valGroup) => {
        const tmpGroups = [...hasGroups]
        const index = tmpGroups.findIndex(group => group.key === currGroup.key)
        tmpGroups[index].groupId = valGroup
        tmpGroups[index].submited = false
        tmpGroups[index].isEdited = true
        setHasGroups(tmpGroups)
    }

    const onChangeDate = (currGroup, type, dateObj, dateString) => {
        const tmpGroups = [...hasGroups]
        const index = tmpGroups.findIndex(group => group.key === currGroup.key)
        tmpGroups[index][type] = dateString
        tmpGroups[index].submited = false
        tmpGroups[index].isEdited = true
        setHasGroups(tmpGroups)
    }

    const onChangeCheck = (currGroup, checked) => {
        const tmpGroups = [...hasGroups]
        const index = tmpGroups.findIndex(group => group.key === currGroup.key)
        tmpGroups[index].userGroupStatus = checked
        tmpGroups[index].submited = false
        tmpGroups[index].isEdited = true
        setHasGroups(tmpGroups)
    }

    const submitGroup = (currGroup) => {
        const tmpGroups = [...hasGroups]
        const index = tmpGroups.findIndex(group => group.key === currGroup.key)
        tmpGroups[index].submited = true
        if (!tmpGroups[index].groupId || tmpGroups[index].startDate === "" || tmpGroups[index].endDate === "") {
            message.warning("Please fill all required field.")
            tmpGroups[index].warning = true
            tmpGroups[index].submited = false
        }else{
            tmpGroups[index].warning = false
        }

        if (!currGroup.isCurrent) {
            setHasGroups(tmpGroups)
            const reqData = {
                ...tmpGroups[index],
                userId: userId
            }  
            RestClient.post(`/trx/user-groups/save`, reqData)
            .then(({ data }) => {
                const resp = data
                if (resp.status) {
                    message.success(resp.message)
                }else{
                    message.error(resp.error ? resp.error : resp.message)
                }
            }).catch(error => {
                message.error(error.message)
            })
        }else{
            message.warning("Update is comming soon!")
        }
    }

    return (
        <Content
            className="site-layout-background content"
        >
            <ButtonMain
                config={btnConfig}
                bottomDashed={true}
            />
            <UserInfo detailUser={detailUser} />
            <Divider orientation="left">
                {`${detailUser.fullname}'s Groups`} {<Tooltip title="Add Group" placement="right"><Button type="primary" icon={<PlusOutlined />} shape="circle" onClick={addGroup} /></Tooltip>}
            </Divider>
            {hasGroups.map((group, i) => (
                <Row gutter={16} key={i} style={{ marginBottom: '10px' }} className={group.warning && 'field__warning'}>
                    <Col className="gutter-row" span={6}>
                        <Select value={group.groupId && group.groupId} style={{ width: '100%' }} onChange={(val) => onChangeGroup(group, val) } placeholder="Group">
                            {groups.map((group, i) => <Option value={group.id} key={i}>{group.group_name}</Option>)}
                        </Select>
                    </Col>
                    <Col className="gutter-row" span={6}>
                        <DatePicker onChange={(date, dateString) => onChangeDate(group, "startDate", date, dateString)} style={{ width: '100%' }} placeholder="Start Date" value={group.startDate && moment(group.startDate, 'YYYY-MM-DD')} />
                    </Col>
                    <Col className="gutter-row" span={6}>
                        <DatePicker onChange={(date, dateString) => onChangeDate(group, "endDate", date, dateString)} style={{ width: '100%' }} placeholder="End Date" value={group.endDate && moment(group.endDate, 'YYYY-MM-DD')} />
                    </Col>
                    <Col className="gutter-row" span={6}>
                        <Space>
                            <Checkbox onChange={(e) => onChangeCheck(group, e.target.checked)} checked={group.userGroupStatus}>Is Active?</Checkbox>
                            {!group.isCurrent && 
                                <Tooltip title="Remove Group" placement="right">
                                    <Popconfirm title="Sure to delete?" onConfirm={() => removeGroup(group)}>
                                        <Button type="danger" icon={<DeleteOutlined />} shape="circle" />
                                    </Popconfirm>
                                </Tooltip>}
                            {!group.submited && 
                                <Tooltip title="Submit Group" placement="right"><Button type="success" icon={<CheckOutlined />} shape="circle" onClick={() => submitGroup(group)} /></Tooltip>}
                        </Space>
                    </Col>
                </Row>
            ))}
        </Content>
    )
}

export default UserHasGroup
