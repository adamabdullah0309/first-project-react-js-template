import React, { useState, useEffect } from 'react'
import { useParams, useHistory } from "react-router-dom"
import { Layout, Divider, Row, Col, List, Checkbox, Spin, Button, Tooltip, Typography } from 'antd'

import { MenuOutlined } from '@ant-design/icons';

import ButtonMain from '../../../components/ButtonMain'
import UserInfo from '../../../components/UserInfo'
import TableManageApp from '../../../components/TableManageApp'

import { api as helperApi, RestClient } from '../../../helpers/api'
import helperGeneral from '../../../helpers/general'

const { Content} = Layout;

function UserHasApp(props) {
    const history = useHistory();
    const { userId } = useParams()
    const [detailUser, setDetailUser] = useState({})
    const [listApp, setListApp] = useState([])
    const [selectedApps, setSelectedApps] = useState([])
    const [currentSelectedApp, setCurrentSelectedApp] = useState(null)
    const [spinInTable, setSpinInTable] = useState(false)

    const btnConfig = {
        discard: {
            type: 'danger',
            onClick: (e) => {
                history.push("/users/access")
            },
        },
        submit: {
            type: 'success',
            onClick: (e) => {
                console.log(selectedApps)
            },
        }
    }

    const columnsManageApp = [
        {
            title: 'Menu',
            render: rowData => {
                return rowData.menu && <Checkbox onChange={(e) => onChangeAppMaster(e, rowData, 'menu')}>{rowData.menu}</Checkbox>;
            },
        },
        {
            title: 'Sub Menu',
            render: rowData => {
                return rowData.subMenu && <Checkbox onChange={(e) => onChangeAppMaster(e, rowData, 'subMenu')}>{rowData.subMenu}</Checkbox>
            },
        },
        {
            title: 'Tab',
            render: rowData => {
                return rowData.tab && <Checkbox onChange={(e) => onChangeAppMaster(e, rowData, 'tab')}>{rowData.tab}</Checkbox>
            },
        },
        {
            title: 'Sub Tab',
            render: rowData => {
                return rowData.subTab && <Checkbox onChange={(e) => onChangeAppMaster(e, rowData, 'subTab')}>{rowData.subTab}</Checkbox>
            },
        },
        {
            title: 'Action',
            render: rowData => {
                return rowData.action && <Checkbox onChange={(e) => onChangeAppMaster(e, rowData, 'action')}>{rowData.action}</Checkbox>
            },
        },
        {
            title: 'Sub Action',
            render: rowData => {
                return rowData.subAction && <Checkbox onChange={(e) => onChangeAppMaster(e, rowData, 'subAction')}>{rowData.subAction}</Checkbox>
            },
        },
    ];

    useEffect(() => {
        helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/users?column=user_uuid&value=${userId}`, {
            headers: helperApi.hubApi.defaultHeaderReq
        }).then(resp => {
            if (resp.status) {
                if (resp.data.length > 0) {
                    setDetailUser(resp.data[0])
                }
            }
        })
    }, [])

    useEffect(() => {
        helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/apps/all`, {
            headers: helperApi.hubApi.defaultHeaderReq
        }).then(resp => {
            if (resp.status) {
                setListApp(resp.data.map(obj => ({ ...obj, checked: false })))
            }
        })
    }, [])

    const onChangeAppMaster = (e, row, type) => {
        let newType = `data${helperGeneral.capitalizeFirstLetter(type)}`
        row[newType].checked = e.target.checked
    }

    const onchangeApp = (e, item) => {
        const currentApps = [...selectedApps]
        const index = currentApps.findIndex(obj => obj.apps_uuid === item.apps_uuid)
        if (e.target.checked) {
            if (index === -1) {
                item.checked = e.target.checked
                setSpinInTable(true)
                populateMasterApp(item)
            }else{
                currentApps[index].checked = e.target.checked
                setSelectedApps(currentApps)
            }
        }else{
            if (index !== -1) {
                currentApps[index].checked = e.target.checked
                setSelectedApps(currentApps)
            }
            setCurrentSelectedApp(null)

        }
    }

    const populateMasterApp = async (app) => {
        const appId = app.apps_uuid
        if (!app.masterList) {
            const msMenu = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/menus?column=apps_uuid&value=${appId}&isParent=0`, {
                headers: helperApi.hubApi.defaultHeaderReq
            });

            let dataMa = []
            if (msMenu.status) {
                for (let i = 0; i < msMenu.data.length; i++) {
                    const element = msMenu.data[i];
                    const fix = { dataMenu: element, menu: element.menu_name }

                    dataMa.push(fix)
                    
                    const msSubMenu = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/menus?column=apps_uuid&value=${appId}&isParent=${element.id}`, {
                        headers: helperApi.hubApi.defaultHeaderReq
                    });
                    if (msSubMenu.status) {
                        for (let j = 0; j < msSubMenu.data.length; j++) {
                            const elementSubMenu = msSubMenu.data[j];
                            const idx = dataMa.findIndex(obj => obj.dataMenu && obj.dataMenu.id === elementSubMenu.menu_parent)
                            
                            if (dataMa[idx].subMenu) {
                                dataMa.push({ dataSubMenu: elementSubMenu, subMenu: elementSubMenu.menu_name })
                            }else{
                                dataMa[idx].dataSubMenu = elementSubMenu
                                dataMa[idx].subMenu = elementSubMenu.menu_name
                            }

                            const msTab1 = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/tabs?column=menu_id&value=${elementSubMenu.id}&isParent=0`, {
                                headers: helperApi.hubApi.defaultHeaderReq
                            });
                            if (msTab1.status) {
                                for (let k = 0; k < msTab1.data.length; k++) {
                                    const elementTab = msTab1.data[k];
                                    const idxSubMenu = dataMa.findIndex(obj => obj.dataSubMenu && obj.dataSubMenu.id === elementSubMenu.id)
                                    
                                    if (dataMa[idxSubMenu].tab) {
                                        dataMa.push({ dataTab: elementTab, tab: elementTab.tab_name })
                                    }else{
                                        dataMa[idxSubMenu].dataTab = elementTab
                                        dataMa[idxSubMenu].tab = elementTab.tab_name
                                    }
                                    
                                    const msSubTab1 = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/tabs?column=menu_id&value=${elementSubMenu.id}&isParent=${elementTab.id}`, {
                                        headers: helperApi.hubApi.defaultHeaderReq
                                    });
                                    if (msSubTab1.status) {
                                        for (let l = 0; l < msSubTab1.data.length; l++) {
                                            const elementSubTab = msSubTab1.data[l];
                                            const idxTab = dataMa.findIndex(obj => obj.dataTab && obj.dataTab.id === elementTab.id)

                                            if (dataMa[idxTab].subTab) {
                                                dataMa.push({ dataSubTab: elementSubTab, subTab: elementSubTab.tab_name })
                                            }else{
                                                dataMa[idxTab].dataSubTab = elementSubTab
                                                dataMa[idxTab].subTab = elementSubTab.tab_name
                                            }

                                            const msAction3 = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/actions?column=menu_id&value=${elementSubMenu.id}&isMain=true`, {
                                                headers: helperApi.hubApi.defaultHeaderReq
                                            });
                                            if (msAction3.status) {
                                                for (let l = 0; l < msAction3.data.length; l++) {
                                                    const elementAction3 = msAction3.data[l];
                                                    let idxSubMenu = dataMa.findIndex(obj => obj.dataSubMenu && obj.dataSubMenu.id === elementSubMenu.id)
                                                    if (dataMa[idxSubMenu].action) {
                                                        dataMa.push({ dataAction: elementAction3, action: elementAction3.action_name })
                                                    }else{
                                                        dataMa[idxSubMenu].dataAction = elementAction3
                                                        dataMa[idxSubMenu].action = elementAction3.action_name
                                                    }
                                                }
                                            }

                                            const msAction4 = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/actions?column=tab_id&value=${elementSubTab.id}&isMain=false`, {
                                                headers: helperApi.hubApi.defaultHeaderReq
                                            });
                                            if (msAction4.status) {
                                                for (let l = 0; l < msAction4.data.length; l++) {
                                                    const elementAction4 = msAction4.data[l];
                                                    let idxSubTab = dataMa.findIndex(obj => obj.dataSubTab && obj.dataSubTab.id === elementSubTab.id)
                                                    if (dataMa[idxSubTab].subAction) {
                                                        dataMa.push({ dataSubAction: elementAction4, subAction: elementAction4.action_name })
                                                    }else{
                                                        dataMa[idxSubTab].dataSubAction = elementAction4
                                                        dataMa[idxSubTab].subAction = elementAction4.action_name
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    const msTab2 = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/tabs?column=menu_id&value=${element.id}&isParent=${element.menu_parent}`, {
                        headers: helperApi.hubApi.defaultHeaderReq
                    });
                    if (msTab2.status) {
                        for (let k = 0; k < msTab2.data.length; k++) {
                            const elementTab2 = msTab2.data[k];
                            let idxMenu = dataMa.findIndex(obj => obj.dataMenu && obj.dataMenu.id === element.id)
                            if (dataMa[idxMenu].tab) {
                                dataMa.push({ dataTab: elementTab2, tab: elementTab2.tab_name })
                            }else{
                                dataMa[idxMenu].dataTab = elementTab2
                                dataMa[idxMenu].tab = elementTab2.tab_name
                            }

                            const msAction1 = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/actions?column=tab_id&value=${elementTab2.id}&isMain=false`, {
                                headers: helperApi.hubApi.defaultHeaderReq
                            });
                            if (msAction1.status) {
                                for (let l = 0; l < msAction1.data.length; l++) {
                                    const elementAction1 = msAction1.data[l];
                                    let idxMenu = dataMa.findIndex(obj => obj.dataTab && obj.dataTab.id === elementTab2.id)
                                    if (dataMa[idxMenu].subAction) {
                                        dataMa.push({ dataSubAction: elementAction1, subAction: elementAction1.action_name })
                                    }else{
                                        dataMa[idxMenu].dataSubAction = elementAction1
                                        dataMa[idxMenu].subAction = elementAction1.action_name
                                    }
                                }
                            }
                        }
                    }
                    
                    const msAction2 = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/actions?column=menu_id&value=${element.id}&isMain=true`, {
                        headers: helperApi.hubApi.defaultHeaderReq
                    });
                    if (msAction2.status) {
                        for (let l = 0; l < msAction2.data.length; l++) {
                            const elementAction2 = msAction2.data[l];
                            let idxMenu = dataMa.findIndex(obj => obj.dataMenu && obj.dataMenu.id === element.id)
                            if (dataMa[idxMenu].action) {
                                dataMa.push({ dataAction: elementAction2, action: elementAction2.action_name })
                            }else{
                                dataMa[idxMenu].dataAction = elementAction2
                                dataMa[idxMenu].action = elementAction2.action_name
                            }
                        }
                    }
                }
            }
        
            // here change selected app
            app.masterList = dataMa
            const newApps = [...selectedApps, app]
            setSelectedApps(newApps)
        }
        setSpinInTable(false)
    }

    const showCurrentApp = (app) => {
        const currentApps = [...selectedApps]
        const index = currentApps.findIndex(obj => obj.apps_uuid === app.apps_uuid)
        setCurrentSelectedApp(currentApps[index])
    }

    return (
        <Content
            className="site-layout-background content"
        >
            <ButtonMain
                config={btnConfig}
                bottomDashed={true}
            />
            <UserInfo detailUser={detailUser} />
            <Divider orientation="left">
                {detailUser.fullname && `${detailUser.fullname}'s Applications`}
            </Divider>
            <Row gutter={16}>
                <Col className="gutter-row" span={7}>
                    <Spin tip="loading..." spinning={spinInTable}>
                        <List
                            header={<div className="title__header-list">List Application</div>}
                            bordered
                            dataSource={listApp}
                            renderItem={app => (
                                <List.Item>
                                        <div className="title__item--checkbox">
                                            <Checkbox onChange={(e) => onchangeApp(e, app)}>{app.apps_name}</Checkbox>
                                            {app.checked && 
                                                <Tooltip title="Show Menu">
                                                    <Button type="primary" shape="circle" size="small" icon={<MenuOutlined />} onClick={() => showCurrentApp(app)} />
                                                </Tooltip>}
                                        </div>
                                </List.Item>
                            )}
                            pagination={{
                                pageSize: 5,
                            }}
                            />
                    </Spin>
                </Col>
                <Col className="gutter-row" span={17}>
                {currentSelectedApp &&
                    <Spin tip="loading..." spinning={spinInTable}>
                        <Typography.Title level={3}>{currentSelectedApp.apps_name}</Typography.Title>
                        <TableManageApp
                            columns={columnsManageApp}
                            dataSource={currentSelectedApp.masterList ? currentSelectedApp.masterList : []}
                        />
                    </Spin>
                }
                </Col>
            </Row>
        </Content>
    )
}

export default UserHasApp
