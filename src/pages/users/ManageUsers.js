import React, { useState, useEffect } from 'react'
import { Layout, Table, Spin, Button, Space, Tooltip, Tag } from 'antd'

import { GroupOutlined, AppstoreOutlined } from '@ant-design/icons'

import { useCookies } from 'react-cookie'

import { Link, useRouteMatch } from "react-router-dom";

import ButtonMain from '../../components/ButtonMain'

import { api as helperApi, RestClient } from '../../helpers/api'

const { Content } = Layout;

function ManageUsers() {
    const [cookies] = useCookies(['sessionUser'])
    const [dataSource, setDataSource] = useState([])
    const [loading, setLoading] = useState(false)

    const match = useRouteMatch();

    const columns = [
        {
            title: 'Fullname',
            dataIndex: 'fullname',
            key: 'fullname',
        },
        {
            title: 'Has Apps',
            dataIndex: 'hasApps',
            key: 'hasApps',
            render: (text, record) => (
                record.apps && record.apps.map(app => (
                    <Tag color="green" key={app.app_uuid}>
                        {app.apps_name.toUpperCase()}
                    </Tag>
                ))
            ),
        },
        {
            title: 'Has Groups',
            dataIndex: 'hasGroups',
            key: 'hasGroups',
            render: (text, record) => (
                record.groups && record.groups.map(group => (
                    <Tag color="geekblue" key={group.group_id}>
                        {group.group_name.toUpperCase()}
                    </Tag>
                ))
            ),
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) =>
                dataSource.length >= 1 ? (
                    <Space>
                        <Tooltip placement="top" title={<span>Manage Access Apps</span>}>
                            <Link to={`${match.url}/apps/${record.user_uuid}`}>
                                <Button type="warning" shape="circle" icon={<AppstoreOutlined />} />
                            </Link>
                        </Tooltip>
                        <Tooltip placement="top" title={<span>Manage Access Groups</span>}>
                            <Link to={`${match.url}/groups/${record.user_uuid}`}>
                                <Button type="primary" shape="circle" icon={<GroupOutlined />} />
                            </Link>
                        </Tooltip>
                    </Space>
                ) : null,
        },
    ]

    const btnConfigAccess = {}

    useEffect(() => {
        populateUsers()
    }, [])

    const populateUsers = () => {
        setLoading(true)
        helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/trx/user-groups?column=client_uuid&value=${cookies.sessionUser.client_uuid}`, {
            headers: helperApi.hubApi.defaultHeaderReq
        }).then( async (resp) => {
            if (resp.status) {
                const ds = resp.data.map(user => ({ ...user, key: user.user_uuid}))
                setDataSource(ds)
            }
            setLoading(false)
        })
    }

    return (
        <Content
            className="site-layout-background content"
        >
            <ButtonMain
                config={btnConfigAccess}
                bottomDashed={true}
            />
            <Spin tip="Loading..." spinning={loading}>
                <Table 
                    columns={columns}
                    dataSource={dataSource}
                />
            </Spin>
        </Content>
    )
}

export default ManageUsers
