import React, { useState, useEffect } from 'react'
import { Layout, Table, Button, Spin, Form, message } from 'antd'
import { EditOutlined } from '@ant-design/icons'

import { useCookies } from 'react-cookie'

import ButtonMain from '../../components/ButtonMain'
import UserForm from './UserForm'

import { api as helperApi, RestClient } from '../../helpers/api'
import helperGeneral from '../../helpers/general'

const { Content} = Layout;

function Users() {
    const [cookies] = useCookies(['sessionUser'])
    const [dataSource, setDataSource] = useState([])
    const [loading, setLoading] = useState(false)
    const [visibleForm, setVisibleForm] = useState(false)
    const [isEditForm, setIsEditForm] = useState(false)
    const [currentEdited, setCurrentEdited] = useState({})
    const [form] = Form.useForm()

    const columns = [
        {
            title: 'Username',
            dataIndex: 'username',
            key: 'username',
        },
        {
            title: 'Name',
            dataIndex: 'fullname',
            key: 'fullname',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Phone Number',
            dataIndex: 'phone_number',
            key: 'phone_number',
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) =>
                dataSource.length >= 1 ? (
                    <Button type="warning" shape="circle" icon={<EditOutlined />} onClick={() => onEditForm(record)} />
                ) : null,
        },
    ]

    const btnConfigUsers = {
        create: {
            type: 'primary',
            onClick: (e) => {
                form.resetFields()
                setVisibleForm(true)
            },
        },
    }

    useEffect(() => {
        populateUsers()
    }, [])

    const populateUsers = () => {
        setLoading(true)
        helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/users?column=client_uuid&value=${cookies.sessionUser.client_uuid}`, {
            headers: helperApi.hubApi.defaultHeaderReq
        }).then(resp => {
            if (resp.status) {
                const ds = resp.data.map(user => ({ ...user, key: user.user_uuid}))
                setDataSource(ds)
            }
            setLoading(false)
        })
    }

    const onSubmitForm = () => {
        const key = 'updatable';
        form.validateFields()
        .then((values) => {
            message.loading({ content: 'Loading...', key });
            if (!helperGeneral.validateStrongPassword(values.password)) {
                message.error('Please enter valid password.');
                form.resetFields(['password']);
                return
            }

            RestClient.post(`/users`, {
                username: values.username,
                fullname: values.fullname,
                password: values.password,
                email: values.email,
                phoneNumber: (values.phoneNumber ? values.phoneNumber : null),
                clientId: cookies.sessionUser.client_uuid
            }).then(({ data }) => {
                const resp = data
                if (resp.status) {
                    message.success({ content: resp.message, key, duration: 4})
                    setVisibleForm(false)
                    form.resetFields();
                    populateUsers();
                }else{
                    message.error({ content: resp.error, key, duration: 4})
                }
            }).catch(error => {
                message.error({ content: error.message, key, duration: 4})
            })
        }).catch((info) => {
            message.error('Please fill all required field.');
        })
    }

    const onEditForm = (record) => {
        setVisibleForm(true)
        setIsEditForm(true)
        setCurrentEdited(record)
        form.setFieldsValue({
            fullname: record.fullname,
            username: record.username,
            email: record.email,
            phoneNumber: record.phone_number
        })
    }

    const onUpdatetForm = () => {
        const key = 'updatable';
        form.validateFields()
        .then((values) => {
            message.loading({ content: 'Loading...', key });

            let reqData = {
                username: values.username,
                fullname: values.fullname,
                email: values.email,
                phoneNumber: (values.phoneNumber ? values.phoneNumber : null),
                userId: currentEdited.user_uuid
            }

            if (values.password) {
                reqData.newPassword = values.password
            }

            RestClient.post(`/users/update`, reqData).then(({ data }) => {
                const resp = data
                if (resp.status) {
                    message.success({ content: resp.message, key, duration: 4})
                    setVisibleForm(false)
                    setIsEditForm(false)
                    form.resetFields();
                    populateUsers();
                }else{
                    message.error({ content: resp.error, key, duration: 4})
                }
            }).catch(error => {
                message.error({ content: error.message, key, duration: 4})
            })
        }).catch((info) => {
            message.error('Please fill all required field.');
        })
    }

    return (
        <Content
            className="site-layout-background content"
        >
            <ButtonMain
                config={btnConfigUsers}
                bottomDashed={true}
            />
            <Spin tip="Loading..." spinning={loading}>
                <Table 
                    columns={columns}
                    dataSource={dataSource}
                />
            </Spin>
            <UserForm 
                form={form}
                visible={visibleForm}
                onClose={() => setVisibleForm(false)}
                onSubmit={onSubmitForm}
                onUpdate={onUpdatetForm}
                isEdit={isEditForm}
            />
        </Content>
    )
}

export default Users
