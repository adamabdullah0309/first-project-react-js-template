import React, { useEffect, useState } from 'react'
import { Row, Col, List, Typography, Checkbox, Alert, Spin } from 'antd'

import { api as helperApi } from '../../helpers/api'
import helperGeneral from '../../helpers/general'

import ButtonMain from '../../components/ButtonMain'
import TableManageApp from '../../components/TableManageApp'

const { Title } = Typography;

function ManageGroup(props) {
    const [dataSource, setDataSource] = useState([])
    const [selectedGroup, setSelectedGroup] = useState('')
    const [spinInTable, setSpinInTable] = useState(false)

    const btnConfigManageGroup = {
        save: {
            type: 'success',
            onClick: (e) => console.log(selectedGroup),
        },
    }

    const columnsManageApp = [
        {
            title: 'Menu',
            render: rowData => {
                return rowData.menu && <Checkbox onChange={(e) => onChangeAppMaster(e, rowData, 'menu')}>{rowData.menu}</Checkbox>;
            },
        },
        {
            title: 'Sub Menu',
            render: rowData => {
                return rowData.subMenu && <Checkbox onChange={(e) => onChangeAppMaster(e, rowData, 'subMenu')}>{rowData.subMenu}</Checkbox>
            },
        },
        {
            title: 'Tab',
            render: rowData => {
                return rowData.tab && <Checkbox onChange={(e) => onChangeAppMaster(e, rowData, 'tab')}>{rowData.tab}</Checkbox>
            },
        },
        {
            title: 'Sub Tab',
            render: rowData => {
                return rowData.subTab && <Checkbox onChange={(e) => onChangeAppMaster(e, rowData, 'subTab')}>{rowData.subTab}</Checkbox>
            },
        },
        {
            title: 'Action',
            render: rowData => {
                return rowData.action && <Checkbox onChange={(e) => onChangeAppMaster(e, rowData, 'action')}>{rowData.action}</Checkbox>
            },
        },
        {
            title: 'Sub Action',
            render: rowData => {
                return rowData.subAction && <Checkbox onChange={(e) => onChangeAppMaster(e, rowData, 'subAction')}>{rowData.subAction}</Checkbox>
            },
        },
    ];

    const populateGroups = () => {
        helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/groups?column=status&value=true`, {
            headers: helperApi.hubApi.defaultHeaderReq
        }).then(resp => {
            if (resp.status) {
                setDataSource(resp.data)
            }
        })
    }

    const populateApps = (groupItem) => {
        helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/apps/all`, {
            headers: helperApi.hubApi.defaultHeaderReq
        }).then(resp => {
            if (resp.status) {
                groupItem.apps = resp.data
                const newGroups = [...dataSource]
                const index = newGroups.findIndex((obj) => groupItem.id === obj.id);
                newGroups[index] = groupItem
                setSelectedGroup(groupItem)
                setDataSource(newGroups)
            }
        })
    }

    useEffect(() => {
        populateGroups()
    }, [])

    const onChangeAppMaster = (e, row, type) => {
        let newType = `data${helperGeneral.capitalizeFirstLetter(type)}`
        row[newType].checked = e.target.checked
    }

    const changeSelectedGroup = (item) => {
        if (!item.apps) {
            populateApps(item)
        } else {
            const newGroups = [...dataSource]
            const index = newGroups.findIndex((obj) => item.id === obj.id);
            newGroups[index] = item
            setSelectedGroup(item)
            setDataSource(newGroups)
        }
    }

    const onChange = (e, app) => {
        const tmpSelectedGroup = selectedGroup
        const newApps = [...tmpSelectedGroup.apps];
        const index = newApps.findIndex((item) => app.id === item.id);
        let item = tmpSelectedGroup.apps[index];
        item.checked = e.target.checked
        tmpSelectedGroup.apps.splice(index, 1, { ...item, ...app });
        setSelectedGroup(tmpSelectedGroup)

        const newGroups = [...dataSource]
        const idx = newGroups.findIndex((obj) => tmpSelectedGroup.id === obj.id);
        newGroups[idx] = tmpSelectedGroup
        setDataSource(newGroups)

        // populate master-apps
        if (e.target.checked) {
            setSpinInTable(true)
            populateMasterApp(app)
        }
    }

    const populateMasterApp = async (app) => {
        const appId = app.apps_uuid
        if (!app.masterList) {
            const msMenu = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/menus?column=apps_uuid&value=${appId}&isParent=0`, {
                headers: helperApi.hubApi.defaultHeaderReq
            });

            let dataMa = []
            if (msMenu.status) {
                for (let i = 0; i < msMenu.data.length; i++) {
                    const element = msMenu.data[i];
                    const fix = { dataMenu: element, menu: element.menu_name }

                    dataMa.push(fix)
                    
                    const msSubMenu = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/menus?column=apps_uuid&value=${appId}&isParent=${element.id}`, {
                        headers: helperApi.hubApi.defaultHeaderReq
                    });
                    if (msSubMenu.status) {
                        for (let j = 0; j < msSubMenu.data.length; j++) {
                            const elementSubMenu = msSubMenu.data[j];
                            const idx = dataMa.findIndex(obj => obj.dataMenu && obj.dataMenu.id === elementSubMenu.menu_parent)
                            
                            if (dataMa[idx].subMenu) {
                                dataMa.push({ dataSubMenu: elementSubMenu, subMenu: elementSubMenu.menu_name })
                            }else{
                                dataMa[idx].dataSubMenu = elementSubMenu
                                dataMa[idx].subMenu = elementSubMenu.menu_name
                            }

                            const msTab1 = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/tabs?column=menu_id&value=${elementSubMenu.id}&isParent=0`, {
                                headers: helperApi.hubApi.defaultHeaderReq
                            });
                            if (msTab1.status) {
                                for (let k = 0; k < msTab1.data.length; k++) {
                                    const elementTab = msTab1.data[k];
                                    const idxSubMenu = dataMa.findIndex(obj => obj.dataSubMenu && obj.dataSubMenu.id === elementSubMenu.id)
                                    
                                    if (dataMa[idxSubMenu].tab) {
                                        dataMa.push({ dataTab: elementTab, tab: elementTab.tab_name })
                                    }else{
                                        dataMa[idxSubMenu].dataTab = elementTab
                                        dataMa[idxSubMenu].tab = elementTab.tab_name
                                    }
                                    
                                    const msSubTab1 = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/tabs?column=menu_id&value=${elementSubMenu.id}&isParent=${elementTab.id}`, {
                                        headers: helperApi.hubApi.defaultHeaderReq
                                    });
                                    if (msSubTab1.status) {
                                        for (let l = 0; l < msSubTab1.data.length; l++) {
                                            const elementSubTab = msSubTab1.data[l];
                                            const idxTab = dataMa.findIndex(obj => obj.dataTab && obj.dataTab.id === elementTab.id)

                                            if (dataMa[idxTab].subTab) {
                                                dataMa.push({ dataSubTab: elementSubTab, subTab: elementSubTab.tab_name })
                                            }else{
                                                dataMa[idxTab].dataSubTab = elementSubTab
                                                dataMa[idxTab].subTab = elementSubTab.tab_name
                                            }

                                            const msAction3 = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/actions?column=menu_id&value=${elementSubMenu.id}&isMain=true`, {
                                                headers: helperApi.hubApi.defaultHeaderReq
                                            });
                                            if (msAction3.status) {
                                                for (let l = 0; l < msAction3.data.length; l++) {
                                                    const elementAction3 = msAction3.data[l];
                                                    let idxSubMenu = dataMa.findIndex(obj => obj.dataSubMenu && obj.dataSubMenu.id === elementSubMenu.id)
                                                    if (dataMa[idxSubMenu].action) {
                                                        dataMa.push({ dataAction: elementAction3, action: elementAction3.action_name })
                                                    }else{
                                                        dataMa[idxSubMenu].dataAction = elementAction3
                                                        dataMa[idxSubMenu].action = elementAction3.action_name
                                                    }
                                                }
                                            }

                                            const msAction4 = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/actions?column=tab_id&value=${elementSubTab.id}&isMain=false`, {
                                                headers: helperApi.hubApi.defaultHeaderReq
                                            });
                                            if (msAction4.status) {
                                                for (let l = 0; l < msAction4.data.length; l++) {
                                                    const elementAction4 = msAction4.data[l];
                                                    let idxSubTab = dataMa.findIndex(obj => obj.dataSubTab && obj.dataSubTab.id === elementSubTab.id)
                                                    if (dataMa[idxSubTab].subAction) {
                                                        dataMa.push({ dataSubAction: elementAction4, subAction: elementAction4.action_name })
                                                    }else{
                                                        dataMa[idxSubTab].dataSubAction = elementAction4
                                                        dataMa[idxSubTab].subAction = elementAction4.action_name
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    const msTab2 = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/tabs?column=menu_id&value=${element.id}&isParent=${element.menu_parent}`, {
                        headers: helperApi.hubApi.defaultHeaderReq
                    });
                    if (msTab2.status) {
                        for (let k = 0; k < msTab2.data.length; k++) {
                            const elementTab2 = msTab2.data[k];
                            let idxMenu = dataMa.findIndex(obj => obj.dataMenu && obj.dataMenu.id === element.id)
                            if (dataMa[idxMenu].tab) {
                                dataMa.push({ dataTab: elementTab2, tab: elementTab2.tab_name })
                            }else{
                                dataMa[idxMenu].dataTab = elementTab2
                                dataMa[idxMenu].tab = elementTab2.tab_name
                            }

                            const msAction1 = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/actions?column=tab_id&value=${elementTab2.id}&isMain=false`, {
                                headers: helperApi.hubApi.defaultHeaderReq
                            });
                            if (msAction1.status) {
                                for (let l = 0; l < msAction1.data.length; l++) {
                                    const elementAction1 = msAction1.data[l];
                                    let idxMenu = dataMa.findIndex(obj => obj.dataTab && obj.dataTab.id === elementTab2.id)
                                    if (dataMa[idxMenu].subAction) {
                                        dataMa.push({ dataSubAction: elementAction1, subAction: elementAction1.action_name })
                                    }else{
                                        dataMa[idxMenu].dataSubAction = elementAction1
                                        dataMa[idxMenu].subAction = elementAction1.action_name
                                    }
                                }
                            }
                        }
                    }
                    
                    const msAction2 = await helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/actions?column=menu_id&value=${element.id}&isMain=true`, {
                        headers: helperApi.hubApi.defaultHeaderReq
                    });
                    if (msAction2.status) {
                        for (let l = 0; l < msAction2.data.length; l++) {
                            const elementAction2 = msAction2.data[l];
                            let idxMenu = dataMa.findIndex(obj => obj.dataMenu && obj.dataMenu.id === element.id)
                            if (dataMa[idxMenu].action) {
                                dataMa.push({ dataAction: elementAction2, action: elementAction2.action_name })
                            }else{
                                dataMa[idxMenu].dataAction = elementAction2
                                dataMa[idxMenu].action = elementAction2.action_name
                            }
                        }
                    }
                }
            }
        
            const tmpSelectedGroup = selectedGroup
            const newApps = [...tmpSelectedGroup.apps];
            const index = newApps.findIndex((item) => app.id === item.id);
            let item = tmpSelectedGroup.apps[index];
            item.masterList = dataMa
            tmpSelectedGroup.apps.splice(index, 1, { ...item, ...app });
            setSelectedGroup(tmpSelectedGroup)
            
            const newGroups = [...dataSource]
            const idx = newGroups.findIndex((obj) => tmpSelectedGroup.id === obj.id);
            newGroups[idx] = tmpSelectedGroup
            setDataSource(newGroups)
        }
        setSpinInTable(false)
    }

    return (
        <>
            <ButtonMain 
                config={btnConfigManageGroup}
                bottomDashed={true}
            />
            <Row gutter={16}>
                <Col className="gutter-row" span={6}>
                    <List
                        header={<div className="title__header-list">List Group</div>}
                        bordered
                        dataSource={dataSource}
                        renderItem={item => (
                            <List.Item>
                                <div className={`title__item-list ${selectedGroup.id === item.id && 'title__item-list--active'}`} onClick={() => changeSelectedGroup(item)}>{item.group_name}</div>
                            </List.Item>
                        )}
                    />
                </Col>
                <Col className="gutter-row" span={18}>
                    <div>
                        {selectedGroup &&
                            <>
                                <Title level={3}>
                                    {selectedGroup.group_name && selectedGroup.group_name}
                                </Title>
                                <Alert message="Any changes to a Group must be saved" type="warning" />
                                <div style={{ marginTop: '5px' }}>
                                    {selectedGroup.apps && selectedGroup.apps.map(app =>
                                        <div key={app.id}>
                                            <Checkbox onChange={(e) => onChange(e, app)} checked={app.checked && app.checked}>{app.apps_name}</Checkbox>
                                            {app.checked &&
                                                <Spin tip="loading..." spinning={spinInTable}>
                                                    <TableManageApp
                                                        columns={columnsManageApp}
                                                        dataSource={app.masterList ? app.masterList : []}
                                                    />
                                                </Spin>
                                            }
                                        </div> 
                                    )}
                                </div>
                            </>
                        }
                    </div>
                </Col>
            </Row>
        </>
    )
}

export default ManageGroup
