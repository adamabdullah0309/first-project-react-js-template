import React, { useContext, useState, useEffect, useRef } from 'react'
import { Layout, Table, Input, Button, Popconfirm, Form, Tabs, AutoComplete, Spin } from 'antd'
import { DeleteOutlined } from '@ant-design/icons';

import { api as helperApi } from '../../helpers/api'

import ButtonMain from '../../components/ButtonMain'
import ManageGroup from './ManageGroup'

const { Content} = Layout;
const { TabPane } = Tabs;

const EditableContext = React.createContext();

const EditableRow = ({ index, ...props }) => {
    const [form] = Form.useForm();
    return (
        <Form form={form} component={false}>
            <EditableContext.Provider value={form}>
                <tr {...props} />
            </EditableContext.Provider>
        </Form>
    );
};

const EditableCell = ({ title, editable, children, dataIndex, fieldInput, record, handleSave, ...restProps }) => {
    const [editing, setEditing] = useState(false);
    const inputRef = useRef();
    const form = useContext(EditableContext);

    useEffect(() => {
        if (editing && fieldInput !== 'select') {
            inputRef.current.focus();
        }
    }, [editing, fieldInput]);
  
    const toggleEdit = () => {
        setEditing(!editing);
        form.setFieldsValue({
            [dataIndex]: record[dataIndex],
        });
    };
  
    const save = async (e) => {
        try {
            const values = await form.validateFields();
            toggleEdit();
            handleSave({ ...record, ...values });
        } catch (errInfo) {
            console.log('Save failed:', errInfo);
        }
    };

    const onSelect = async (data) => {
        const values = await form.validateFields();
        toggleEdit();
        handleSave({ ...record, ...values });
    };
  
    let childNode = children;
  
    if (editable) {
        childNode = editing ? (
            <Form.Item
                style={{
                    margin: 0,
                }}
                name={dataIndex}
                rules={[
                    {
                    required: true,
                    message: `${title} is required.`,
                    },
                ]}
            >
                {fieldInput === 'select' ? 
                    <AutoComplete
                        ref={inputRef}
                        options={[
                            {
                                label: 'Active',
                                value: 'Active'
                            },
                            {
                                label: 'In Active',
                                value: 'In Active'
                            }
                        ]}
                        onSelect={onSelect}
                        onSearch={save}
                        placeholder="Select Status"
                    /> :     
                    <Input ref={inputRef} onPressEnter={save} onBlur={save} placeholder="Input Group Name" />
                }
            </Form.Item>
        ) : (
            <div
                className="editable-cell-value-wrap"
                style={{
                    paddingRight: 24,
                }}
                onClick={toggleEdit}
            >
                {children}
            </div>
        );
    }
  
    return <td {...restProps}>{childNode}</td>;
};

function Groups() {
    const [dataSource, setDataSource] = useState([])
    const [count, setCount] = useState(0)
    const [loading, setLoading] = useState(false)

    const columns = [
        {
            title: 'Group Name',
            dataIndex: 'groupName',
            width: '30%',
            editable: true,
        },
        {
            title: 'Status',
            dataIndex: 'groupStatus',
            editable: true,
            fieldInput: 'select'
        },
        {
            title: 'Action',
            dataIndex: 'action',
            render: (text, record) =>
                dataSource.length >= 1 ? (
                    <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record.key)}>
                        <Button type="danger" shape="circle" icon={<DeleteOutlined />} />
                    </Popconfirm>
                ) : null,
        },
    ];

    const btnConfigGroups = {
        create: {
            type: 'primary',
            onClick: (e) => handleAdd(),
        },
        submit: {
            type: 'success',
            onClick: (e) => console.log(dataSource),
        },
    }

    useEffect(() => {
        setLoading(true)
        helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/groups/all`, {
            headers: helperApi.hubApi.defaultHeaderReq
        }).then(resp => {
            if (resp.status) {
                const ds = resp.data.map(group => ({
                    key: group.id,
                    groupId: group.id,
                    groupName: group.group_name,
                    groupStatus: group.status ? 'Active' : 'In Active',
                    status: group.status
                }))
                setDataSource(ds)
                setCount(ds.length)
            }
            setLoading(false)
        })
    }, [])

    const handleDelete = (key) => {
        const tmpDataSource = dataSource.filter((item) => item.key !== key);
        setDataSource(tmpDataSource);
    };

    const handleAdd = () => {
        const newCount = count + 1
        const newData = {
            key: newCount,
            groupName: '',
            groupStatus: '',
        };
        setCount(newCount)
        setDataSource([...dataSource, newData])
    };

    const handleSave = (row) => {
        const newData = [...dataSource];
        const index = newData.findIndex((item) => row.key === item.key);
        const item = newData[index];
        newData.splice(index, 1, { ...item, ...row });
        setDataSource(newData)
    };

    const components = {
        body: {
            row: EditableRow,
            cell: EditableCell,
        },
    };

    const columnsFix = columns.map((col) => {
        if (!col.editable) {
            return col;
        }
  
        return {
            ...col,
                onCell: (record) => ({
                record,
                editable: col.editable,
                dataIndex: col.dataIndex,
                title: col.title,
                fieldInput: col.fieldInput,
                handleSave: handleSave,
            }),
        };
    });

    const callbackTab = (key) => console.log(`Tab ${key}`);

    return (
        <Content
            className="site-layout-background content"
        >
            <Tabs onChange={callbackTab} type="card">
                <TabPane tab="Groups" key="1">
                    <ButtonMain 
                        config={btnConfigGroups}
                        bottomDashed={true}
                    />
                    <Spin tip="Loading..." spinning={loading}>
                        <Table
                            components={components}
                            rowClassName={() => 'editable-row'}
                            bordered
                            dataSource={dataSource}
                            columns={columnsFix}
                            pagination={{ current: Math.ceil(dataSource.length / 10) }}
                        />
                    </Spin>
                </TabPane>
                <TabPane tab="Manage Groups" key="2">
                    <ManageGroup />
                </TabPane>
            </Tabs>
        </Content>
    )
}

export default Groups
