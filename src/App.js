import React, { useEffect, useState } from 'react'
import { Layout } from 'antd'

import { Switch, Route } from "react-router-dom";
import { useCookies } from 'react-cookie'

import Sidebar from './components/Sidebar'
import Navbar from './components/Navbar'

import helperGeneral from './helpers/general'
import { api as helperApi } from './helpers/api'
import routes from './helpers/route'

const { Footer } = Layout;
const Dates = new Date()

function App() {
	const [cookies, setCookie, removeCookie] = useCookies(['sessionUser'])
	const [collapsed, setCollapsed] = useState(false)

	const getDetailUser = () => {
		helperApi.hubApi.request(helperApi.hubApi.detailUserUrl, {
			headers: helperApi.hubApi.defaultHeaderReq
		}).then(resp => {
			if (resp.status) {
				setCookie('sessionUser', resp.data, { path: '/',  })
			} else {
				logout()
				window.location.href = `${helperApi.hubApi.loginUrl}?redirectURL=${window.location.origin}`
			}
		})
	}

	useEffect(() => {
		if (localStorage.getItem('accessCode') && !cookies.sessionUser) {
			getDetailUser()
		}
	})

	useEffect(() => {
		if (!localStorage.getItem('accessCode')) {
			const accessCode = helperGeneral.getParameterByName('accessCode')
			if (accessCode) {
				localStorage.setItem("accessCode", accessCode);
			}
			window.location.href = `${helperApi.hubApi.loginUrl}?redirectURL=${window.location.origin}` 
		} else {
			const accessCode = helperGeneral.getParameterByName('accessCode')
			if (accessCode) {
				localStorage.setItem("accessCode", accessCode);
			}
		}
	})

	const changeCollapsed = () => setCollapsed(!collapsed)

	const logout = () => {
		localStorage.removeItem("accessCode");
		removeCookie('sessionUser')
	}

	return (
		<Layout>
			<Sidebar 
				collapsed={collapsed}
			/>
			<Layout className="site-layout">
				<Navbar
					collapsed={collapsed}
					changeCollapsed={changeCollapsed}
					logout={logout}
					lastSession={localStorage.getItem('accessCode')}
					sessionUser={cookies.sessionUser}
				/>
				<Switch>
					{routes.map((route, i) => <Route key={i} path={route.path} component={route.component} exact={route.exact} /> )}
				</Switch>
				<Footer style={{ textAlign: 'center' }}>User Management ©{Dates.getFullYear()} Powered by ProAcc</Footer>
			</Layout>
		</Layout>
	);
}

export default App;
