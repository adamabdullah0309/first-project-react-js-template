const sideMenu = [

    {
        path: '#',
        title: 'Users',
        child: [
            {
                path: '/users',
                title: 'List User',
            },
            {
                path: '/users/access',
                title: 'Manage Access',
            },
        ]
    },
    {
        path: '#',
        title: 'Setting',
        child: [
            {
                path: '/settings/master-setting',
                title: 'Master Setting',
            }
        ]
    }
]

export default sideMenu