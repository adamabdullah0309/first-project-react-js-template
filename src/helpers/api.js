import axios from 'axios'

const api = {
    hubApi: {
        baseUrl: process.env.REACT_APP_BASEURL_PROACCESS,
        baseUrlApi: `${process.env.REACT_APP_BASEURL_PROACCESS}/api`,
        loginUrl: `${process.env.REACT_APP_BASEURL_PROACCESS}/apps/login`,
        logoutUrl: `${process.env.REACT_APP_BASEURL_PROACCESS}/apps/clear-session`,
        detailUserUrl: `${process.env.REACT_APP_BASEURL_PROACCESS}/apps/me`,
        defaultHeaderReq: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-access-code': localStorage.getItem('accessCode')
        },
        request: async (url, options = {}) => {
            const response = await fetch(url, options);
            return response.json()
        }
    }
}

const RestClient = axios.create({
    baseURL: api.hubApi.baseUrlApi,
    timeout: 10000,
    headers: api.hubApi.defaultHeaderReq
});

export { api, RestClient }