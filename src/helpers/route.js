import Home from '../pages/Home'
import Groups from '../pages/groups/Groups'
import Users from '../pages/users/Users'
import MasterSetting from '../pages/Setting/MasterSetting/MasterSetting'
import ManageUsers from '../pages/users/ManageUsers'
import UserHasApp from '../pages/users/access/UserHasApp'
import UserHasGroup from '../pages/users/access/UserHasGroup'

const routes = [
    {
        path: '/',
        title: 'Dashboard',
        exact: true,
        component: Home,
    },
    {
        path: '/groups',
        title: 'Groups',
        exact: true,
        component: Groups,
    },
    {
        path: '/settings/master-setting',
        title: 'Master Setting',
        exact: true,
        component: MasterSetting,
    },
    {
        path: '/users',
        title: 'Users',
        exact: true,
        component: Users,
    },
    {
        path: '/users/access',
        title: 'Manage Users',
        exact: true,
        component: ManageUsers,
    },
    {
        path: '/users/access/groups/:userId',
        title: 'User has Groups',
        exact: true,
        component: UserHasGroup,
    },
    {
        path: '/users/access/apps/:userId',
        title: 'User has Apps',
        exact: true,
        component: UserHasApp,
    },
]

export default routes