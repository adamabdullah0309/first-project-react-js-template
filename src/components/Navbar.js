import React from 'react'
import { Layout, Dropdown, Menu } from 'antd'
import {
	MenuUnfoldOutlined,
	MenuFoldOutlined,
    SettingOutlined,
    UserOutlined,
    LockOutlined,
} from '@ant-design/icons'

import { api as helperApi } from '../helpers/api'

const { Header } = Layout;

const MenuRight = (logout) => (
	<Menu>
        <Menu.Item key="1" icon={<UserOutlined />}>
            Profile
        </Menu.Item>
        <Menu.Item key="2" icon={<LockOutlined />}>
            <a href={`${helperApi.hubApi.logoutUrl}?redirectURL=${window.location.origin}`} onClick={logout}>
                Logout
            </a>
        </Menu.Item>
	</Menu>
);

function Navbar(props) {
    return (
        <Header className="site-layout-background menu-header" style={{ padding: 0 }}>
            {React.createElement(props.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                className: 'trigger',
                onClick: props.changeCollapsed,
            })}
            <Dropdown.Button 
                className="menu-header--right"
                overlay={MenuRight(props.logout)}
                type="proacc"
                placement="bottomLeft"
                icon={<SettingOutlined />}
            >
                {props.sessionUser && props.sessionUser.fullname}
            </Dropdown.Button>
        </Header>
    )
}

export default Navbar
