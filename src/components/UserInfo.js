import React from 'react'
import { Descriptions } from 'antd'
import momnet from 'moment'

function UserInfo({ detailUser }) {
    return (
        <Descriptions title="User Info" bordered>
            <Descriptions.Item label="Fullname">{detailUser.fullname}</Descriptions.Item>
            <Descriptions.Item label="Username">{detailUser.username}</Descriptions.Item>
            <Descriptions.Item label="Telephone">{detailUser.phone_number}</Descriptions.Item>
            <Descriptions.Item label="Email">{detailUser.email}</Descriptions.Item>
            <Descriptions.Item label="Created Date">{momnet.utc(detailUser.created_date).format("dddd, Do MMMM YYYY, HH:mm:ss A")}</Descriptions.Item>
        </Descriptions>
    )
}

export default UserInfo
