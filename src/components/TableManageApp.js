import React from 'react'

function TableManageApp(props) {
    return (
        <>
            <table className="table__custom">
                <thead>
                    <tr className="table__row">
                        {props.columns.map((col, i) => <th className="table__col" key={col.id ? col.id : i}>{col.title}</th>)}
                    </tr>
                </thead>
                <tbody>
                    {props.dataSource.map((data, i) => (
                        <tr key={i} className="table__row">
                            {props.columns.map((col, j) => <td className="table__col" key={col.id ? col.id : j}>{col.render(data)}</td>)}
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    )
}

export default TableManageApp
