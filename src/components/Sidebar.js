import React from 'react'
import { Layout, Menu } from 'antd'
import { MenuOutlined } from '@ant-design/icons';
import { Link } from "react-router-dom";

import sideMenu from '../helpers/sideMenu'

const { Sider } = Layout;
const { SubMenu } = Menu;

function Sidebar(props) {
    return (
        <Sider 
            trigger={null}
            collapsible
            collapsed={props.collapsed}
            className={`background-sider ${props.collapsed ? '' : 'set-width'}`}
            >
            <div className="logo" />
            <Menu 
                theme="dark"
                mode="inline"
                className="list-menu-sidebar"
                // defaultSelectedKeys={['1']}
                >
                    {sideMenu.map((menu, i) => (
                        <React.Fragment key={`list-${i}`}>
                            {menu.child ? 
                                <SubMenu key={`sub-${i}`} icon={<MenuOutlined />} title={menu.title} className="link">
                                    {menu.child.map((submenu, j) => 
                                        <Menu.Item key={`sub-${i}-${j}`}>
                                            <Link to={submenu.path} className="link">
                                                {submenu.title}
                                            </Link>
                                        </Menu.Item>
                                    )}
                                </SubMenu>
                            : 
                                <Menu.Item key={i} icon={<MenuOutlined />}>
                                    <Link to={menu.path} className="link">
                                        {menu.title}
                                    </Link>
                                </Menu.Item>
                            }
                        </React.Fragment>
                    ))}
            </Menu>
        </Sider>
    )
}

export default Sidebar
