import React, { useState, useEffect } from 'react'
import { Button, Divider } from 'antd';
import { api as helperApi } from '../helpers/api'

function ButtonMain({ config, topDashed, bottomDashed }) {
    const [buttons, setButtons] = useState([])

    useEffect(() => {
        helperApi.hubApi.request(`${helperApi.hubApi.baseUrlApi}/actions/buttons`, {
			headers: helperApi.hubApi.defaultHeaderReq
		}).then(resp => {
            if (resp.status) {
                setButtons(resp.data)
            }
        })
    }, [])

    return (
        <>
            {topDashed && <Divider dashed />}
            {buttons.map((button, i) => 
                <React.Fragment key={i}>
                    <Button 
                        {...typeof config[button.button_name.toLowerCase()] !== 'undefined' && config[button.button_name.toLowerCase()]}
                        disabled={typeof config[button.button_name.toLowerCase()] === 'undefined'}
                    >
                        {button.button_name}
                    </Button>
                    <Divider type="vertical" />
                </React.Fragment>
            )}
            {bottomDashed && <Divider dashed />}
        </>
    )
}

export default ButtonMain
